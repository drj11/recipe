[brutal]: #xdate "2023-07-05"
[brutal]: #xauthor "drj"

# Yorkshire Pudding

## Ingredients

- Oil
- 75g plain white flour
- 2 eggs
- 75ml milk (animal milk is best)
- 55ml water (see note below about alternate measuring)

## Method

Preheat a fan oven to 210°C.
Smear oil around each cup of a 12-cup Yorkshire pudding tin.
Warm the oiled tin in the oven for at least 10 minutes,
to get the fat hot before pouring the batter in.

With an electric mixer,
whisk all the ingredients, until
the batter is smooth and just a little airy.
Take out the tin from the oven and deftly pour a ⅟12 portion of batter
into each cup.
Return the laden tin to the oven swiftly and smoothly, avoid knocks.
Bake for 19 minutes.

Note on milk and water: The quantities of milk and water don’t
need to be particularly precise.
I often make it in a measuring jug and make up with milk
until the 290ml line is reached.

# END
