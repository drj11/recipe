Malteser Cake

100g margarine
3 tablespoons syrup
225g chocolate (milk or plain)
200g digestives, crushed
200g maltesers
100g plain chocolate (for top)

Grease cake tin;
Put the margarine, syrup, & choc into a pan;
heat until melted.
Remove from the heat & stir in the crushed biscuits & maltesers.
Press into tin & leave in cool place to set.
Put melted chocolate on top.

