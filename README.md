# Design for recipe cards.

Design units are millimetres, mm.
Inkscape can set fonts in mm.

Reverse (white on black) banner stripe, 25mm deep.
Title white on black on banner stripe.

Title is Andika New Basic Regular 10mm,
set solid (100% baseline-to-baseline; no leading).

Body, ingredients: Andika New Basic 4/4.5mm;
method: Andika New Basic 4/5mm.
