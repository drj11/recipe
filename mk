#!/bin/sh
# For rsvg-convert, see apt install librsvg2-bin
set -e
mkdir -p png
mkdir -p pdf

rm -f png/* pdf/*

for svg in *.svg
do
    outpng=$(basename "$svg" .svg).png
    outpdf=$(basename "$svg" .svg).pdf
    inkscape --export-background --export-area-page --export-type=png --export-filename="png/${outpng}" "${svg}"
    inkscape --export-background --export-area-page --export-margin=2 --export-type=pdf --export-filename="pdf/${outpdf}" "${svg}"
done
pdfunite pdf/*.pdf $(basename "$PWD").pdf
